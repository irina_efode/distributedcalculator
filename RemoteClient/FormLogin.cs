﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Collections;
using RemoteBase;

namespace RemoteClient
{
    public partial class FormLogin : Form
    {
        TcpChannel channel;
        FormClient objCalculator;

        public FormLogin()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            loginCalculator();
        }

        private void loginCalculator()
        {
            if (channel == null)
            {
                channel = new TcpChannel();
                ChannelServices.RegisterChannel(channel, false);
                objCalculator = new FormClient();
                objCalculator.remoteObj = (SampleObject)Activator.GetObject(typeof(RemoteBase.SampleObject), localHostTextBox1.Text);
                this.Hide();
            }
            objCalculator.Show();
        }
    }
}
