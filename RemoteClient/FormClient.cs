﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Collections;
using RemoteBase;

namespace RemoteClient
{
    public partial class FormClient : Form
    {
        internal SampleObject remoteObj;

        public FormClient()
        {
            InitializeComponent();
        }

        private void resultButton_Click(object sender, EventArgs e)
        {
            bool text1, text2;
            text1 = (remoteObj != null && num1textBox.Text.Trim().Length > 0);
            text2 = (remoteObj != null && num2textBox.Text.Trim().Length > 0);
            if (text1 && text2)
                resLabel.Text = (remoteObj.divNum(float.Parse(num1textBox.Text), float.Parse(num2textBox.Text))).ToString();
            else
                resLabel.Text = "Error! Uncorrect data!";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        

    }


}
