﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace RemoteBase
{
    public class SampleObject : MarshalByRefObject
    {
        public float divNum(float num1, float num2)
        {
            return (num1 / num2);
        }

    }
}