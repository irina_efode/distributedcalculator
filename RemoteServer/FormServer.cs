﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using RemoteBase;

namespace RemoteServer
{
    public partial class Form1 : Form
    {
        TcpChannel channel;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //starting of working Server
        private void startButton_Click(object sender, EventArgs e)
        {
            if (channel == null)
            {
                channel = new TcpChannel(8080);
                ChannelServices.RegisterChannel(channel, false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(SampleObject), "Calculator", WellKnownObjectMode.Singleton);
                statusLabel.Text = "Success! Server is running!";
                startButton.Enabled = false;
                stopButton.Enabled = true;
            }
        }

        //stopping of working Server
        private void stopButton_Click(object sender, EventArgs e)
        {
            if (channel != null)
            {
                ChannelServices.UnregisterChannel(channel);
                channel = null;
                statusLabel.Text = "Server is stopped";
                startButton.Enabled = true;
                stopButton.Enabled = false;
            }
        }
    }
}
