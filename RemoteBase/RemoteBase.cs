﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace RemoteBase
{
    public class SampleObject : MarshalByRefObject
    {
        //Realization of method "divNum" for dividing entered numbers
        public float divNum(float num1, float num2)
        {
            return (num1 / num2);
        }

    }
}